﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BUS;
using DTA;

namespace HotelMNS_Web
{
    public partial class Booking : System.Web.UI.Page
    {
        RoomAvaiableBUS roomBus = new RoomAvaiableBUS();
        protected List<spGetListRoomAvaiAble_Result> resultListRoom;
        protected List<spGetServiceRoom_Result> resultServiceRoom;
        protected List<spGetFacilitiesRoom_Result> resultFacilitiesRoom;
        protected List<spGetListImageRoom_Result> resultImageroom;
        public DateTime DateArrival;
        public DateTime DateDepart;
        public int adulNumber;
        public int childNumber;
        public int totalDays;
        public int countRoom;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["dateArial"]))
                {
                    DateTime? startDate = DateTime.Parse(Request.QueryString[0]);
                    txtDateArrival.Text = DateTime.Parse(Request.QueryString[0]).ToShortDateString();
                    DateTime? endDate = DateTime.Parse(Request.QueryString[1]);
                    txtDateDeparture.Text = DateTime.Parse(Request.QueryString[1]).ToShortDateString();
                    int? roomCount = Int32.Parse(Request.QueryString[2]);
                    dropdownPickRoom.Text = roomCount.ToString();
                    int adultNumber = Int32.Parse(Request.QueryString[3]);
                    dropdownAdultNumber.Text = adultNumber.ToString();
                    int childrenNUmber = Int32.Parse(Request.QueryString[4]);
                    dropdownChilNumber.Text = childrenNUmber.ToString();
                    GetListRoom(startDate, endDate, adultNumber, childrenNUmber, roomCount);
                }
                else
                {
                    txtDateArrival.Text = DateTime.Now.ToShortDateString();
                    txtDateDeparture.Text = DateTime.Now.AddDays(1).ToShortDateString();
                    LoadRoomFirst();
                }
            }
        }
        protected void GetListRoom(DateTime? statdate, DateTime? endDate, int adultCount, int ChildCount, int? roomCount)
        {
            int? capa = adultCount + ChildCount;
            resultListRoom = roomBus.getListInfoRoom(statdate, endDate, capa, roomCount);
            DateTime a = (System.DateTime)statdate;
            DateTime b = (System.DateTime)endDate;
            var span = b.Subtract(a).TotalDays;
            totalDays = Int32.Parse(span.ToString());
            DateArrival = a;
            DateDepart = b;
            adulNumber= adultCount;
            childNumber = ChildCount;
            countRoom = (int)roomCount;
        }
        protected void btnSubmitRoom_Click(object sender, EventArgs e)
        {
            DateTime? startDate = DateTime.Parse(txtDateArrival.Text);
            DateTime? endDate = DateTime.Parse(txtDateDeparture.Text);
            int? roomCount = Int32.Parse(dropdownPickRoom.Text);
            int adultNumber = Int32.Parse(dropdownAdultNumber.Text);
            int childrenNUmber = Int32.Parse(dropdownChilNumber.Text);
            GetListRoom(startDate, endDate, adultNumber, childrenNUmber, roomCount);
            
        }
        protected void LoadRoomFirst()
        {
            DateTime? startDate = DateTime.Now;
            DateTime? endDate = DateTime.Now.AddDays(1);
            int? roomCount = 1;
            int AdultNUmber = 1;
            int childrenNumber = 0; 
            GetListRoom(startDate, endDate, AdultNUmber, childrenNumber, roomCount);
        }
        public void LoadServiceRoom(int? roomtypeId, int? categoryId)
        {
            resultServiceRoom = roomBus.GetServiceRoom(roomtypeId, categoryId);
        }
        public void LoadFacilites(int? roomtypeId, int? categoryid)
        {
            resultFacilitiesRoom = roomBus.GetFacilities(roomtypeId, categoryid);
        }
        public void LoadListRoomImage(int? roomtypeid, int? roomcateid)
        {
            resultImageroom = roomBus.GetImageRoom(roomtypeid, roomcateid);
        }
    }
}