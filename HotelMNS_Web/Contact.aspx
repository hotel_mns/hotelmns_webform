﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Contact.aspx.cs" Inherits="HotelMNS_Web.Contact" ClientIDMode="Static" %>


<asp:Content ID="contactHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="pull-right">
        <div class="pull-left">
            <nav class="nav">
                <ul id="navigate" class="sf-menu navigate">
                    <li><a href="Default.aspx">HOMEPAGE</a>
                    </li>
                    <li><a href="Accommondation.aspx">ACCOMMODATION</a>
                    </li>
                    <li><a href="News.aspx">NEWS</a>
                    </li>
                    <li><a href="About.aspx">ABOUT</a></li>
                    <li class="active"><a href="Contact.aspx">CONTACT</a></li>
                </ul>
            </nav>
        </div>
        <div class="pull-right">
            <div class="button-style-1 margint45">
                <a href="Booking.aspx"><i class="fa fa-calendar"></i>BOOK NOW</a>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="ConactContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="breadcrumb breadcrumb-1 pos-center">
        <h1 style="color:white;">CONTACT</h1>
    </div>
    <div class="content">
        <!-- Content Section -->
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-4 margint60">
                    <!-- Sidebar -->
                    <div class="luxen-widget news-widget">
                        <div class="title">
                            <h5>HOTEL INFORMATION</h5>
                        </div>
                        <p>Curabitur blandit tempus porttitor. Nulla vitae elit libero, a pharetra augue. Lorem ipsumero, a pharetra augue. Lorem ipsum dolor sit amet, consectedui.</p>
                    </div>
                    <div class="luxen-widget news-widget">
                        <div class="title">
                            <h5>CONTACT</h5>
                        </div>
                        <ul class="footer-links">
                            <li>
                                <p><i class="fa fa-map-marker"></i>Lorem ipsum dolor sit amet lorem Victoria 8011 Australia </p>
                            </li>
                            <li>
                                <p><i class="fa fa-phone"></i>+61 3 8376 6284 </p>
                            </li>
                            <li>
                                <p><i class="fa fa-envelope"></i>info@2035themes.com</p>
                            </li>
                        </ul>
                    </div>
                    <div class="luxen-widget news-widget">
                        <div class="title">
                            <h5>SOCIAL MEDIA</h5>
                        </div>
                        <ul class="social-links">
                            <li><a href="http://facebook.com/2035themes"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="http://twitter.com/2035themes"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="http://vine.com/2035themes"><i class="fa fa-vine"></i></a></li>
                            <li><a href="http://foursquare.com/2035themes"><i class="fa fa-foursquare"></i></a></li>
                            <li><a href="http://instagram.com/2035themes"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9 col-sm-8">
                    <!-- Contact -->
                    <div id="map" class="maps pos-center margint60">
                        <!-- Contact Maps -->

                    </div>
                    <div class="contact-form margint60">
                        <!-- Contact Form -->
                        <input type="text" placeholder="Name" name="name">
                        <input type="text" placeholder="Subject" name="subject">
                        <input type="text" placeholder="E-Mail" name="email">
                        <textarea placeholder="Write what do you want..." name="message"></textarea>
                        <input class="pull-right margint10" type="submit" value="SUBMIT">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
