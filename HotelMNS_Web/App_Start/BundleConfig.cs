﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace HotelMNS_Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Conten/css").Include(
                            "~/Content/bootstrap.min.css",
                            "~/Content/flexslider.css",
                            "~/Content/prettyPhoto.css",
                            "~/Content/datepicker.css",
                            "~/Content/selectordie.css",
                            "~/Content/main.css",
                            "~/Content/2035.responsive.css"));
            bundles.Add(new ScriptBundle("~/Script/js").Include(
                           "~/Scripts/jquery-1.11.1.min.js",
                           "~/Scripts/bootstrap.min.js",
                           "~/Scripts/retina-1.1.0.min.js",
                           "~/Scripts/jquery.flexslider-min.js",
                           "~/Scripts/superfish.pack.1.4.1.js",
                           "~/Scripts/jquery.prettyPhoto.js",
                           "~/Scripts/bootstrap-datepicker.js",
                           "~/Scripts/selectordie.min.js",
                           "~/Scripts/jquery.slicknav.min.js",
                           "~/Scripts/jquery.parallax-1.1.3.js",
                           "~/Scripts/main.js"
                           ));
        }
    }
}