﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ThankBooking.aspx.cs" Inherits="HotelMNS_Web.ThankBooking" EnableEventValidation="false" %>

<asp:Content ID="ThankBookingHeader" ContentPlaceHolderID="HeaderContent" runat="server">
        <div class="pull-right">
        <div class="pull-left">
            <nav class="nav">
                <ul id="navigate" class="sf-menu navigate">
                    <li><a href="Default.aspx">HOMEPAGE</a>
                    </li>
                    <li><a href="Accommondation.aspx">ACCOMMODATION</a>
                    </li>
                    <li><a href="News.aspx">NEWS</a>
                    </li>
                    <li><a href="About.aspx">ABOUT</a></li>
                    <li><a href="Contact.aspx">CONTACT</a></li>
                </ul>
            </nav>
        </div>
    </div>
</asp:Content>
<asp:Content ID="ThankBookingContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="breadcrumb breadcrumb-1 pos-center">
        <h2 style="color:white;">THANK YOU FOR CHOOSE MY HOTEL FOR YOUR TRIP</h2>
            <h2 style="color:white;">HOPE YOU HAVE GREAT TIME !</h2>
    </div>
</asp:Content>
