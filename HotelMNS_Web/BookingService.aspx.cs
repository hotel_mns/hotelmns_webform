﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BUS;
using System.Collections;
using System.Net.Mail;
using System.Net;
using System.IO;
using DTA;

namespace HotelMNS_Web
{
    public partial class BookingService : System.Web.UI.Page
    {
        protected RoomAvaiableBUS listroom = new RoomAvaiableBUS();
        protected List<DTA.spGetServiceRoom_Result> listservice;
        protected List<DTA.spGetFacilitiesRoom_Result> listFacilities;
        protected List<spGetListImageRoom_Result> resultImageroom;
        protected string roomName;
        protected int roomPrice;
        protected int shroomPrice;
        protected string roomDescription;
        protected int countNight;
        protected int RoomCount;
        protected DateTime shDateArrival;
        protected DateTime shDateDepart;
        protected int shAdultNumber;
        protected int shChilNumber;
        protected int shRoomCount;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                ShowPickRoom();
        }

        protected void ShowPickRoom()
        {
            int roomType = Int32.Parse(Request.QueryString[0]);
            int roomCate = Int32.Parse(Request.QueryString[1]);
            roomName = Request.QueryString[2];
            shDateArrival = DateTime.Parse(Request.QueryString[4]);
            shDateDepart = DateTime.Parse(Request.QueryString[5]);
            shAdultNumber = Int32.Parse(Request.QueryString[6].ToString());
            shChilNumber = Int32.Parse(Request.QueryString[7].ToString());
            countNight = Int32.Parse(Request.QueryString[8].ToString());
            RoomCount = Int32.Parse(Request.QueryString[9].ToString());
            roomPrice = countNight * Int32.Parse(Request.QueryString[3])*RoomCount;
            shroomPrice = roomPrice;
            listservice = listroom.GetServiceRoom(roomType, roomCate);
            listFacilities = listroom.GetFacilities(roomType, roomCate);
            resultImageroom = listroom.GetImageRoom(roomType, roomCate);
        }

        protected void btnBookingRoom_Click(object sender, EventArgs e)
        {
            int roomType = Int32.Parse(Request.QueryString[0]);
            int roomCate = Int32.Parse(Request.QueryString[1]);
            DateTime dArrival = DateTime.Parse(Request.QueryString[4]);
            DateTime dDepart = DateTime.Parse(Request.QueryString[5]);
            int aduNumber = Int32.Parse(Request.QueryString[6].ToString());
            int chiNumber = Int32.Parse(Request.QueryString[7].ToString());
            int totalNight = Int32.Parse(Request.QueryString[8].ToString());
            int totalRoom = Int32.Parse(Request.QueryString[9].ToString());
            int totalPrice = totalNight * Int32.Parse(Request.QueryString[3])* totalRoom;
            string firstName = txtFirstName.Text;
            string lastName = txtLasName.Text;
            string kindGender = dropdownGender.Text;
            bool gender = (kindGender == "Male") ? true : false;
            string city = dropdownCity.Text;
            string email = txtEmail.Text;
            string phoneNumber = txtPhoneNumber.Text;
            string note = txtNote.Text;
            decimal roomprice = (decimal)totalPrice;
            int CustomerId = listroom.GettNewCustomer(firstName, lastName, gender, email, phoneNumber, city);
            int bookingId = listroom.InsertNewCustomerBooking(dArrival, dDepart, chiNumber, aduNumber, note, CustomerId, roomprice);
            listroom.InsertNewCustomerBookingRoom(dArrival, dDepart, roomType, roomCate, bookingId, totalRoom);
            SendMail(firstName, lastName, kindGender, city, email, phoneNumber, note, dArrival, dDepart, aduNumber, chiNumber, totalRoom, totalPrice);
            Response.Redirect("ThankBooking.aspx");
        }
        protected void SendMail(string fistname, string lastname, string kindGender, string city, string email, string phoneNumber, string note, DateTime dateArrvail, DateTime dateDepart, int adulNum, int chilNum, int totalRoom, int totalPrice)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("dtranthuy1995@gmail.com");
            msg.To.Add(email);
            msg.Subject = "Testing";
            msg.Body = CreateBody(fistname,lastname,kindGender,city,email,phoneNumber,note,dateArrvail,dateDepart,adulNum,chilNum,totalRoom,totalPrice);
            msg.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            NetworkCredential netcred = new NetworkCredential();
            netcred.UserName = "dtranthuy1995@gmail.com";
            netcred.Password = "bachkhoadn17";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = netcred;
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.Send(msg);
        }

        protected string CreateBody(string fistname, string lastname, string kindGender, string city, string email, string phoneNumber, string note, DateTime dateArrvail, DateTime dateDepart, int adulNum, int chilNum, int totalRoom, int totalPrice)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailCheckInfo.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{lname}", lastname);
            body = body.Replace("{name}",fistname+" "+lastname);
            body = body.Replace("{gender}", kindGender);
            body = body.Replace("{city}", city);
            body = body.Replace("{email}", email);
            body = body.Replace("{phonenumber}", phoneNumber);
            body = body.Replace("{notes}", note);
            body = body.Replace("{datearrival}", dateArrvail.ToShortDateString());
            body = body.Replace("{datedepart}", dateDepart.ToShortDateString());
            body = body.Replace("{datedepart}", adulNum.ToString());
            body = body.Replace("{adult}", adulNum.ToString());
            body = body.Replace("{children}", chilNum.ToString());
            body = body.Replace("{roomCount}", totalRoom.ToString());
            body = body.Replace("{roomPrice}", totalPrice.ToString());
            return body;
        }
    }
}