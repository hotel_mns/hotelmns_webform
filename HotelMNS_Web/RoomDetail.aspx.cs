﻿using DTA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BUS;

namespace HotelMNS_Web
{
    public partial class RoomDetail : System.Web.UI.Page
    {
        RoomAvaiableBUS roomBus = new RoomAvaiableBUS();
        protected List<spGetServiceRoom_Result> resultServiceRoom;
        protected spGetRoomCapacility_Result resultCapaRoom;
        protected List<spGetListImageRoom_Result> resultImageRoom;
        protected string roomName;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                LoadRoomInfo();
        }
        
        public void LoadRoomInfo()
        {
            int roomtyeid = Int32.Parse(Request.QueryString[0]);
            int roomcateid = Int32.Parse(Request.QueryString[1]);
            roomName = Request.QueryString[2] + " " + Request.QueryString[3];
            LoadServiceRoom(roomtyeid, roomcateid);
            LoadCapaRoom(roomtyeid, roomcateid);
            LoadImageRoom(roomtyeid, roomcateid);
        }

        public void LoadServiceRoom(int roomtyeid, int roomcateid)
        {
            resultServiceRoom = roomBus.GetServiceRoom(roomtyeid, roomcateid);
        }
        public void LoadCapaRoom(int roomtyeid, int roomcateid)
        {
            resultCapaRoom = roomBus.GetCapaRoom(roomtyeid, roomcateid);
        }
        public void LoadImageRoom(int roomtypeid, int roomcateid)
        {
            resultImageRoom = roomBus.GetImageRoom(roomtypeid, roomcateid);
        }
    }
}