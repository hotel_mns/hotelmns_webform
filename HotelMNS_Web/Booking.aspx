﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Booking.aspx.cs" Inherits="HotelMNS_Web.Booking" EnableEventValidation="false" ClientIDMode="Static"%>

<asp:Content ID="bookingHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="pull-right">
        <div class="pull-left">
            <nav class="nav">
                <ul id="navigate" class="sf-menu navigate">
                    <li><a href="Default.aspx">HOMEPAGE</a>
                    </li>
                    <li><a href="Accommondation.aspx">ACCOMMODATION</a>
                    </li>
                    <li><a href="News.aspx">NEWS</a>
                    </li>
                    <li><a href="About.aspx">ABOUT</a></li>
                    <li><a href="Contact.aspx">CONTACT</a></li>
                </ul>
            </nav>
        </div>
    </div>
</asp:Content>

<asp:Content ID="bookingContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="breadcrumb breadcrumb-1 pos-center">
        <h1 style="color:white;">MAKE YOUR RESERVATION</h1>
    </div>
    <div class="free-book">
        <div class="book-slider">
            <div class="container">
                <div class="row pos-center">
                    <div class="pos-center white-title marginb60">
                        <h2>Search Your Room</h2>
                        <img src="img/shape.png">
                    </div>
                    <div class="reserve-form-area">
                        <form action="#" method="post" id="ajax-reservation-form">
                            <ul class="clearfix">
                                <li class="li-input">
                                    <label>ARRIVAL</label>
                                    <asp:TextBox type="text" ID="txtDateArrival" runat="server" name="dpd1" class="date-selector" placeholder="&#xf073;" />
                                </li>
                                <li class="li-input">
                                    <label>DEPARTURE</label>
                                    <asp:TextBox type="text" ID="txtDateDeparture" runat="server" name="dpd2" class="date-selector" placeholder="&#xf073;" />
                                </li>
                                <li class="li-select">
                                    <label>ROOMS</label>
                                    <asp:DropDownList ID="dropdownPickRoom" runat="server" CssClass="pretty-select">
                                        <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                    </asp:DropDownList>
                                </li>
                                <li class="li-select">
                                    <label>ADULT</label>
                                    <asp:DropDownList ID="dropdownAdultNumber" runat="server" CssClass="pretty-select">
                                        <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                    </asp:DropDownList>
                                </li>
                                <li class="li-select">
                                    <label>CHILDREN</label>
                                    <asp:DropDownList ID="dropdownChilNumber" runat="server" CssClass="pretty-select">
                                        <asp:ListItem Value="0" Selected="True">0</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                    </asp:DropDownList>
                                </li>
                                <li>
                                    <div class="button-style-1 margint30">
                                        <asp:LinkButton ID="btnSubmitRoom" runat="server" OnClick="btnSubmitRoom_Click"><i class="fa fa-search"></i>SEARCH</asp:LinkButton>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <!-- Content Section -->
        <div class="container margint60">
            <div class="row">
                <div class="col-lg-12 marginb20"><!-- Room Sort -->
					<div class="sortby clearfix">
						<div class="pull-left">
							<h3>Search Result: Found <%=resultListRoom.Count %> result avaiable</h3>
						</div>
					</div>	
				</div>
                <div class="col-lg-9">
                    <!-- Explore Rooms -->
                    <table>
                        <tr class="products-title">
                            <td class="table-products-image pos-center">
                                <h6>IMAGE</h6>
                            </td>
                            <td class="table-products-name pos-center">
                                <h6>ROOM NAME</h6>
                            </td>
                            <td class="table-products-price pos-center">
                                <h6>PRICE</h6>
                            </td>
                            <td class="table-products-total pos-center">
                                <h6>PURCHASE</h6>
                            </td>
                        </tr>
                        <%foreach (var item in resultListRoom)
                            { %>
                        <tr class="table-products-list pos-center">
                            <td class="products-image-table">
                                <%LoadListRoomImage(item.roomtype_id, item.roomcategory_id); %>
                                <img alt="Products Image 1" src="<%="Image/"+resultImageroom[0].path_image%>"></td>
                            <td class="title-table">
                                <div class="room-details-list clearfix">
                                    <div class="pull-left">
                                        <h5><%= item.room_type+" "+item.room_category+" Room" %></h5>
                                    </div>
                                    <div class="pull-left room-rating">
                                        <ul>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star inactive"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="list-room-icons clearfix">
                                    <ul>
                                        <%LoadServiceRoom(item.roomtype_id, item.roomcategory_id);
                                            foreach (var ser in resultServiceRoom)
                                            {
                                        %>
                                        <li><i class="<%=ser.image%>"></i></li>
                                        <% } %>
                                        <%LoadFacilites(item.roomtype_id, item.roomcategory_id);
                                            foreach (var fac in resultFacilitiesRoom)
                                            {
                                        %>
                                        <li><i class="<%=fac.image %>"></i></li>
                                        <% } %>
                                        <li>
                                            <p><%=item.roomCount %> room left</p>
                                        </li>
                                    </ul>
                                </div>
                                <p><%=item.description %></p>
                            </td>
                            <td>
                                <h3><%=item.price%>$</h3>
                            </td>
                            <td>
                                <div class="button-style-1">
                                    <a href='BookingService.aspx?roomtyid=<%=item.roomtype_id%>&roomCategoryid=<%=item.roomcategory_id%>&roomName=<%=Server.UrlEncode(item.room_type + " " + item.room_category + " Room")%>&roomPrice=<%=item.price%>&dateArri=<%=Server.UrlEncode(DateArrival.ToShortDateString())%>&dateDepart=<%=Server.UrlEncode(DateDepart.ToShortDateString())%>&aduNumber=<%=adulNumber%>&chiNumber=<%=childNumber%>&totalDay=<%=totalDays%>&roomCount=<%=countRoom%>'>
                                        <i class="fa fa-calendar"></i>
                                        <span class="mobile-visibility">BOOK NOW</span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                    </table>
                </div>
                <div class="col-lg-3">
                    <!-- Sidebar -->
                    <div class="luxen-widget news-widget">
                        <div class="title-quick marginb20">
                            <h5>HOTEL INFORMATION</h5>
                        </div>
                        <p>Curabitur blandit tempus porttitor. Nulla vitae elit libero, a pharetra augue. Lorem ipsumero, a pharetra augue. Lorem ipsum dolor sit amet, consectedui.</p>
                    </div>
                    <div class="luxen-widget news-widget">
                        <div class="title">
                            <h5>CONTACT</h5>
                        </div>
                        <ul class="footer-links">
                            <li>
                                <p><i class="fa fa-map-marker"></i>Lorem ipsum dolor sit amet lorem Victoria 8011 Australia </p>
                            </li>
                            <li>
                                <p><i class="fa fa-phone"></i>+61 3 8376 6284 </p>
                            </li>
                            <li>
                                <p><i class="fa fa-envelope"></i>info@2035themes.com</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
