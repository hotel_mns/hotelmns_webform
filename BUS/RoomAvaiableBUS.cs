﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTA;

namespace BUS
{
    public class RoomAvaiableBUS
    {
        RoomAvaiablDB roomavail = new RoomAvaiablDB();

        public List<spGetListRoomAvaiAble_Result> getListInfoRoom(DateTime? startDate, DateTime? endDate, int? capa,int? roomCount)
        {
            return roomavail.GetInfoRoom(startDate, endDate, capa, roomCount);
        }
        public List<spGetServiceRoom_Result> GetServiceRoom(int? roomType, int? roomCate)
        {
            return roomavail.GetService(roomType, roomCate);
        }
        public List<spGetFacilitiesRoom_Result> GetFacilities(int? roomType, int? roomCate)
        {
            return roomavail.GetFacilities(roomType, roomCate);
        }
        public int GettNewCustomer(string firstName, string lastName, bool gender, string email, string phoneNumber, string citizenship)
        {
            return roomavail.GetCustomer(firstName, lastName, gender, email, phoneNumber, citizenship);
        }
        public int InsertNewCustomerBooking(DateTime checkin, DateTime checkout, int chilNumber, int adultNumber, string note, int customer, decimal price)
        {
            return roomavail.InsertBooking(checkin, checkout, chilNumber, adultNumber, note, customer, price);
        }
        public void InsertNewCustomerBookingRoom(DateTime? checkin, DateTime? checkout, int? roomtyeid, int? roomcateid, int bookingid, int totalRoom)
        {
            roomavail.InsertBookingRoom(checkin, checkout, roomtyeid, roomcateid, bookingid,totalRoom);
        }
        public List<spGetAllRoom_Result> GetAllAccomondation()
        {
            return roomavail.GetAllRoom();
        }
        
        public spGetRoomCapacility_Result GetCapaRoom(int? roomtyeid , int? roomcateid)
        {
            return roomavail.GetCapa(roomtyeid, roomcateid);
        }
        public List<spGetListImageRoom_Result> GetImageRoom(int? roomtypeid, int? roomcateid)
        {
            return roomavail.GetImage(roomtypeid, roomcateid);
        }
    }
}
