﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTA
{
    public class RoomAvaiablDB
    {
        HotelMNSEntities db = new HotelMNSEntities();

        public List<spGetListRoomAvaiAble_Result> GetInfoRoom(DateTime? startDate, DateTime? endDate, int? capa, int? roomCount)
        {
            return db.spGetListRoomAvaiAble(startDate, endDate, capa, roomCount).ToList();
        }

        public List<spGetServiceRoom_Result> GetService(int? roomTypeid, int? roomCateid)
        {
            return db.spGetServiceRoom(roomTypeid, roomCateid).ToList();
        }
        public List<spGetFacilitiesRoom_Result> GetFacilities(int? roomTypeid, int? roomCateid)
        {
            return db.spGetFacilitiesRoom(roomTypeid, roomCateid).ToList();
        }
        public int GetCustomer(string firstName, string lastName, bool gender, string email, string phoneNumber, string citizenship)
        {
            Customer newcustomer = new Customer();
            newcustomer.first_name = firstName;
            newcustomer.last_name = lastName;
            newcustomer.gender = gender;
            newcustomer.email = email;
            newcustomer.phone_number = phoneNumber;
            newcustomer.citizenship = citizenship;
            db.Customers.Add(newcustomer);
            db.SaveChanges();
            return newcustomer.id;
        }
        public int InsertBooking(DateTime checkin, DateTime checkout, int chilNumber, int adultNumber, string note, int customer, decimal price)
        {
            Booking booking = new Booking();
            booking.created_date = DateTime.Now;
            booking.date_checkin = checkin;
            booking.date_checkout = checkout;
            booking.children_number = chilNumber;
            booking.adult_number = adultNumber;
            booking.notes = note;
            booking.Customer = customer;
            booking.Status = 1;
            booking.Prices = price;
            db.Bookings.Add(booking);
            db.SaveChanges();
            return booking.id;
        }

        public void InsertBookingRoom(DateTime? checkin, DateTime? checkout, int? roomtyeid, int? roomcateid, int bookingid, int totalRoom)
        {
            List<spGetRoomId_Result> roomid = db.spGetRoomId(checkin, checkout, roomtyeid, roomcateid).ToList();
            if (totalRoom < roomid.Count)
            {
                Booking_Room bookingroom = new Booking_Room();
                bookingroom.Booking = bookingid;
                Random rd = new Random();
                int i = rd.Next(roomid.Count);
                bookingroom.Room = roomid[i].room_id;
                db.Booking_Room.Add(bookingroom);
                db.SaveChanges();
            }
            else
            {
                foreach (var item in roomid)
                {
                    Booking_Room bookingroom = new Booking_Room();
                    bookingroom.Booking = bookingid;
                    bookingroom.Room = item.room_id;
                    db.Booking_Room.Add(bookingroom);
                    db.SaveChanges();
                }
            }

        }
        public List<spGetAllRoom_Result> GetAllRoom()
        {
            return db.spGetAllRoom().ToList();
        }

        public spGetRoomCapacility_Result GetCapa(int? roomtyeid, int? roomcateid)
        {
            return db.spGetRoomCapacility(roomtyeid, roomcateid).Single();
        }
        public List<spGetListImageRoom_Result> GetImage(int? roomtypeid, int? roomcateid)
        {
            return db.spGetListImageRoom(roomtypeid, roomcateid).ToList();
        }
    }
}
